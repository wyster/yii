<?php

return array(
    // Максимальная ширина фото
    'userPhotoSizeWidth'  => '100',
    // Максимальная высота фото, по желанию
    'userPhotoSizeHeight' => '',
    // Максимальное кол-во рефералов
    'userMaxReferrals'    => '5',
    'domain'              => 'localhost',
);