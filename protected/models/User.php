<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string  $login
 * @property string  $email
 * @property string  $password
 * @property string  $fullName
 * @property string  $address
 * @property string  $photo
 * @property integer $partner
 */
class User extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('login, email, password, fullName', 'required'),
            array('login, email', 'unique'),
            array('email', 'email'),
            array('partner', 'numerical', 'integerOnly' => TRUE),
            array('login, email, password, fullName, address, photo', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, login, email, password, fullName, address, photo, partner', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'       => 'ID',
            'login'    => 'Login',
            'email'    => 'Email',
            'password' => 'Password',
            'fullName' => 'Full Name',
            'address'  => 'Address',
            'photo'    => 'Photo',
            'partner'  => 'Partner',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('login', $this->login, TRUE);
        $criteria->compare('email', $this->email, TRUE);
        $criteria->compare('password', $this->password, TRUE);
        $criteria->compare('fullName', $this->fullName, TRUE);
        $criteria->compare('address', $this->address, TRUE);
        $criteria->compare('photo', $this->photo, TRUE);
        $criteria->compare('partner', $this->partner);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @param string|null $name
     * @return string
     */
    public static function generateFileName($name = NULL)
    {
        $extension = pathinfo($name, PATHINFO_EXTENSION);

        return md5($name . time() . rand()) . '.' . $extension;
    }

    /**
     * @return bool
     */
    public function photoExists()
    {
        if (empty($this->photo)) {
            return FALSE;
        }

        return file_exists(Yii::getPathOfAlias('webroot') . $this->getPhotoPath());
    }

    /**
     * @param bool $thumb
     * @return string
     */
    public function getPhotoPath($thumb = FALSE)
    {
        return Yii::app()->request->baseUrl . '/uploads/' . ($thumb ? 'thumb_' : '') . $this->photo;
    }

    /**
     * @return bool
     */
    public function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord === FALSE) {
                $this->setAttribute('updated', date('Y-m-d H:i:s'));
            } else {
                if ($partner = Yii::app()->request->cookies['partner']) {
                    $tableName = User::model()->tableName();
                    $command = Yii::app()->db->createCommand("SELECT (SELECT COUNT(u2.id) FROM {$tableName} u2 WHERE u2.partner = u.id) AS `count` FROM {$tableName} AS u WHERE u.id = :user");
                    $referrals = $command->bindParam('user', $partner->value)->setFetchMode(PDO::FETCH_OBJ)->queryRow();
                    if ((int)$referrals->count < Yii::app()->params->userMaxReferrals) {
                        $this->setAttribute('partner', $partner->value);
                    }
                    unset(Yii::app()->request->cookies['partner']);
                }
                $this->setAttribute('created', date('Y-m-d H:i:s'));
                $this->setAttribute('password', password_hash($this->password, PASSWORD_DEFAULT));
            }
            if ($photo = CUploadedFile::getInstance($this, 'photo')) {
                $photoName = self::generateFileName($photo->getName());
                $pathToUploads = Yii::getPathOfAlias('webroot') . '/uploads';
                $photo->saveAs($pathToUploads . DIRECTORY_SEPARATOR . $photoName);
                $phpThumb = new \PHPThumb\GD($pathToUploads . DIRECTORY_SEPARATOR . $photoName);
                $phpThumb->adaptiveResize(Yii::app()->params->userPhotoSizeWidth, Yii::app()->params->userPhotoSizeHeight);
                $phpThumb->save($pathToUploads . '/thumb_' . $photoName);
                $this->setAttribute('photo', $photoName);
            } else {
                if ($this->isNewRecord === FALSE) {
                    unset($this->photo);
                }
            }

            return TRUE;
        }

        return FALSE;
    }
}
