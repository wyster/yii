<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<? if (count($topPartners) > 0): ?>
<h3>Топ рефералов</h3>
<ul>
    <? foreach ($topPartners as $partner): ?>
        <? if ((int)$partner->count === 0) continue; ?>
        <li><a href="/user/<?=$partner->id;?>"><?=$partner->fullName;?></a>, кол-во: <?=$partner->count;?></li>
    <? endforeach; ?>
</ul>
<? endif; ?>