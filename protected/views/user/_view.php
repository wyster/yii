<?php
/* @var $this UserController */
/* @var $data User */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('login')); ?>:</b>
    <?php echo CHtml::encode($data->login); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
    <?php echo CHtml::encode($data->email); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('fullName')); ?>:</b>
    <?php echo CHtml::encode($data->fullName); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
    <?php echo CHtml::encode($data->address); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('photo')); ?>:</b>
    <?
    if ($data->photoExists()): ?>
        <img src="<?=$data->getPhotoPath(TRUE); ?>"/>
    <? endif; ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('partner')); ?>:</b>
    <?php echo CHtml::encode($data->partner); ?>
    <br/>

    <b>Реферальная ссылка:</b>
    <a href="http://<?=Yii::app()->params->domain;?>/partner/<?=CHtml::encode($data->id);?>">http://<?=Yii::app()->params->domain;?>/partner/<?=CHtml::encode($data->id);?></a>
    <br/>


</div>