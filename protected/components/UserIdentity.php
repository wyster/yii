<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    private $id;

    public function authenticate()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'login=:login OR email=:login';
        $criteria->params = array(':login' => $this->username);
        $record = User::model()->find($criteria);
        if ($record === NULL) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else if (password_verify($this->password, $record->password) === FALSE) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->id = $record->id;
            $this->errorCode = self::ERROR_NONE;
        }

        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->id;
    }
}